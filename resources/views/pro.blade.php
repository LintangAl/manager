<html>
<head>
  <link href="{{ asset('assets/css/pro.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons">
    <link rel="stylesgeet" href="https://rawgit.com/creativetimofficial/material-kit/master/assets/css/material-kit.css">
</head>

<body class="profile-page">

    <nav class="navbar navbar-color-on-scroll navbar-transparent    fixed-top  navbar-expand-lg "  color-on-scroll="100"  id="sectionsNav">
        <div class="container">
            <div class="navbar-translate">
                <a class="navbar-brand" href="" target="_blank">Manager</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="dropdown nav-item">
                      <a href="javascript:void(0)" id="editUser" data-toggle="modal" data-target='#edit_modal' data-id="{{ Auth::user()->id }}" class="nav-link" aria-expanded="false">
                          <i class="material-icons">edit</i> Edit
                      </a>
                    </li>
      				<li class="nav-item">
      					<a class="nav-link" href="{{ route('logout') }}">
      						<i class="material-icons">logout</i> Logout
      					</a>
      				</li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="page-header header-filter" data-parallax="true" style="background-image:url('');"></div>
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ml-auto mr-auto">
                        <div class="profile">
                                <div class="avatar">
                                    <img src="{{asset('assets/img/M.png')}}" alt="Circle Image" class="img-raised rounded-circle img-fluid">
                                </div>
                                <div class="name">
                                    <h3 class="title">{{ Auth::user()->username }}</h3>
                                    <h6>{{ Auth::user()->roles->roles_name }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="description text-center" style="padding-bottom: 25px">
                        <h6>{{ Auth::user()->status }}</h6>
                        <h6>{{ Auth::user()->email }}</h6>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
                    </div>
                </div>
            </div>
        </div>
	</div>

     <!-- Modal -->
     <div class="modal fade" id="edit_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Data : {{ Auth::user()->username }} </h5>
                    <a href="javascript:void(0)" class="nav-link" data-dismiss="modal">
                        <i class="material-icons">cancel</i>
                    </a>
                </div>
                <form id="userdata" action="{{route('user.update', Auth::user()->id )}}" method="POST">
                    @csrf
                <input type="hidden" id="id" name="id" value="{{ Auth::user()->id }}">
                <div class="modal-body" style="text-align: left">
                    <label class="col-form-label">Username :</label>
                    <input type="text" name="username" id="name" value="{{ Auth::user()->username }}" class="form-control mb-3">
                    <label class="col-form-label">Status :</label>
                    <input type="text" name="status" id="status" value="{{ Auth::user()->status }}" class="form-control mb-3">
                    <label class="col-form-label">Email :</label>
                    <input type="text" name="email" id="email" value="{{ Auth::user()->email }}" class="form-control mb-3">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
           </form>
        </div>
    </div>

	<footer class="footer text-center ">
        <p>Manager-Team</p>
    </footer>

    <!--Script-->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
        <script src="{{asset('assets/js/pro.js')}}"></script>
    <!--/Script-->

</body>
</html>
