<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email</title>
</head>
<body>
    <p>Attention!</p>
    <p>New User has been Joined</p>
    <p>Username : {{ $username }}</p>
    <p>Email : {{ $email }}</p>
    <p>Roles : {{ $rolesname }}</p>
</body>
</html>
