@extends('layouts.layout')
    @section('buttonadd')
    <a class="nav-link"  role="button" id="editUser" data-toggle="modal" data-target='#add_modalUser'>
        <i class="fas fa-user-plus"></i>
    </a>
    @endsection
@section('KOSONG')

<?php if ($data->isEmpty()) { ?>
    <center>
    <div class="" style="padding: 20%">
        <div class="card" style="max-width: 50%">KOSONG</div>
    </div>
    </center>
<?php  } else { ?>

    <div class="card-body navbar-white navbar-light" style="border-radius: 0.25rem">
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Username</th>
                <th scope="col">Status</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Verified_at</th>
                <th scope="col">Registered_at</th>
                <th scope="col">Options</th>
                </tr>
            </thead>
            @foreach ($data as $d)
            <tbody>
                <tr>
                <th scope="row">{{ (($data->currentPage() * 10) - 10) + $loop->iteration  }}</th>
                <td>{{$d->username}}</td>
                <td>{{$d->status}}</td>
                <td>{{$d->email}}</td>
                <td>{{$d->roles->roles_name}}</td>
                <td>{{$d->email_verified_at}}</td>
                <td>{{$d->created_at}}</td>
                <td>      <a href ="{{route('dashboard.profile', $d->id)}}" type="button" class="btn text-success btn-sm"><i class="fas fa-user"></i></a>
                            <a href ="{{route('email.form', $d->id)}}" type="button" class="btn text-warning btn-sm"><i class="fas fa-envelope"></i></a>
                            <a role="button" id="editUser" data-userid="{{ $d->id }}" data-roles="{{$d->roles_id}}" data-username="{{$d->username}}" data-status="{{$d->status}}" data-email="{{$d->email}}"  data-attr="{{ route('dashboard.update', $d->id) }}" data-toggle="modal"  data-target='#edit_modal' class="btn text-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <form action ="{{Route('dashboard.destroy', $d->id)}}" method="get" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn text-danger btn-sm  " onclick="return confirm('Sure?')" ><i class="fas fa-trash"></i></button>
                            </form>


                        </td>
                    </tr>
                </tbody>
                @endforeach
        </table>
    </div>
    <div class="row mt-4">
        <div class="col-12 d-flex justify-content-center">
            <nav aria-label="...">
                <ul class="pagination">
                    {{ $data->links() }}
                </ul>
            </nav>
        </div>
    </div>
        <!--MODAL-->
          <div class="modal fade" id="edit_modal">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title">Edit Data User</h5>
                          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form id="userdata_edit" action="{{route('dashboard.update','test')}}" method="post">
                        {{ method_field('patch') }}
                        {{ csrf_field() }}

                        <div class="modal-body">
                          <input type="hidden" name="id" id="id_edit" value="" class="form-control mb-3" required>
                          <label class="col-form-label">Username :</label>
                          <input type="text" name="username" id="username_edit" value="" class="form-control mb-3" required>
                          <label class="col-form-label">Status :</label>
                          <input type="text" name="status" id="status_edit" value="" class="form-control mb-3">
                          <label class="col-form-label">Roles :</label>
                            <select id="roles_id" name="roles_id" class="form-select" aria-label="Default select example" required>
                                @foreach ($roles as $r)
                                    <option value = "{{ $r->id }}">{{ $r->roles_name }}</option>
                                @endforeach
                            </select>
                          <label class="col-form-label">Email :</label>
                          <input type="text" name="email" id="email_edit" value="" class="form-control mb-3" required>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" id="submit_edit" class="btn btn-primary">Submit</button>
                      </div>
                  </div>
              </form>
              </div>
          </div>
        <!--/MODAL-->

<?php } ?>
@endsection

@push('dashboard-script')
    <script>
        $('#edit_modal').on('show.bs.modal', function (event) {
            console.log('modal opened');
                var button = $(event.relatedTarget);

                var username = button.data('username');
                var email = button.data('email');
                var status = button.data('status');
                var roles = button.data('roles');
                var userid = button.data('userid');

                var modal = $(this);

                modal.find('.modal-body #username_edit').val(username);
                modal.find('.modal-body #status_edit').val(status);
                modal.find('.modal-body #email_edit').val(email);
                modal.find('.modal-body #roles_id').val(roles);
                modal.find('.modal-body #id_edit').val(userid);
        })
    </script>
@endpush

