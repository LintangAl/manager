@extends('layouts.layout')
@section('buttonadd')
<a class="nav-link"  role="button" id="editUser" data-toggle="modal" data-target='#add_modalRole'>
    <i class="fas fa-plus-square"></i>
</a>
@endsection
@section('KOSONG')

<?php if ($roles->isEmpty()) { ?>
    <center>
    <div class="" style="padding: 20%">
        <div class="card" style="max-width: 50%">KOSONG</div>
    </div>
    </center>
<?php  } else { ?>

    <div class="card-body navbar-white navbar-light" style="border-radius: 0.25rem">
        <table class="table table-hover">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Role name</th>
                <th scope="col">Created_at</th>
                <th scope="col">Options</th>
                </tr>
            </thead>
            @foreach ($roles as $r)
            <tbody>
                <tr>
                <th scope="row">{{ (($roles->currentPage() * 10) - 10) + $loop->iteration  }}</th>
                <td>{{$r->roles_name}}</td>
                <td>{{$r->created_at}}</td>
                <td>        <a role="button" id="editRoles" data-rolesid="{{ $r->id }}" data-roles_name="{{$r->roles_name}}" data-attr="{{ route('dashboard.updateroles', $r->id) }}" data-toggle="modal"  data-target='#edit_modal' class="btn text-primary btn-sm"><i class="fas fa-edit"></i></a>
                            <form action ="{{Route('dashboard.destroyroles', $r->id)}}" method="get" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn text-danger btn-sm  " onclick="return confirm('Sure?')" ><i class="fas fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
        </table>
    </div>
    <div class="row mt-4">
        <div class="col-12 d-flex justify-content-center">
            <nav aria-label="...">
                <ul class="pagination">
                    {{ $roles->links() }}
                </ul>
            </nav>
        </div>
    </div>
        <!--MODAL-->
          <div class="modal fade" id="edit_modal">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title">Edit Role</h5>
                          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form id="Role_edit" action="{{route('dashboard.updateroles','test')}}" method="post">
                        {{ method_field('patch') }}
                        {{ csrf_field() }}

                        <div class="modal-body">
                          <input type="hidden" name="roles_id" id="id_edit" value="" class="form-control mb-3">
                          <label class="col-form-label">Role name :</label>
                          <input type="text" name="roles_name" id="roles_name" value="" class="form-control mb-3">
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" id="submit_edit" class="btn btn-primary">Submit</button>
                      </div>
                  </div>
              </form>
              </div>
          </div>
        <!--/MODAL-->

<?php } ?>
@endsection

@push('dashboard-script')
    <script>
        $('#edit_modal').on('show.bs.modal', function (event) {
            console.log('modal opened');
                var button = $(event.relatedTarget);

                var rolesname = button.data('roles_name');
                var rolesid = button.data('rolesid');

                var modal = $(this);

                modal.find('.modal-body #roles_name').val(rolesname);
                modal.find('.modal-body #id_edit').val(rolesid);
        })
    </script>
@endpush

