@extends('layouts.app')

@section('content')

<main class="login-form">
  <div class="cotainer">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">Email</div>
                  <div class="card-body">
                      <form action="{{ route('email.send') }}" method="POST">
                          @csrf
                          <div class="form-group row">
                              <label for="name" class="col-md-4 col-form-label text-md-right">To :</label>
                              <div class="col-md-6">
                                    <input class="form-control" value="{{ $data->email }}" name="email" READONLY>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="email_address" class="col-md-4 col-form-label text-md-right">To username :</label>
                                <div class="col-md-6">
                                    <input class="form-control" value="{{ $data->username }}" name="username" READONLY>
                                </div>
                          </div>
                          <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Email Content :</label>
                            <div class="col-md-6">
                                <textarea id="text" type="text" class="form-control" name="email_content" placeholder="Leave a message here..." ></textarea>
                            </div>
                        </div>
                          <div class="col-md-6 offset-md-4">
                              <button type="submit" class="btn btn-primary">
                                  Send
                              </button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
</main>
@endsection
