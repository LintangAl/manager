# How to use
Clone repository

`git clone https://gitlab.com/LintangAl/manager.git

Masuk ke folder yang baru dibuat (seharusnya namanya `manager`) 

`cd manager`


Duplicate File `.env.example` di Folder `manager` dan ubah menjadi `.env` <br>
Kemudian Isi atau Edit File `.env` dengan Database Credential yang diperlukan

`APP_URL=http://manager.test`

`DB_DATABASE=manager_database`

persiapkan email service bisa menggunakan Mailtrap 
selanjutnya konfigurasikan pada bagian Mail sebagai berikut

- MAIL_MAILER=smtp
- MAIL_HOST=smtp.mailtrap.io
- MAIL_PORT=2525
- MAIL_USERNAME= (username mailtrap anda)
- MAIL_PASSWORD= (password maitrap anda)
- MAIL_ENCRYPTION=tls
- MAIL_FROM_ADDRESS=ManagerAdmin@example.com
- MAIL_FROM_NAME="${APP_NAME}"


Install the composer dependencies

`composer install`


Set application key

`php artisan key:generate`   


Buat Database baru dengan nama `manager_database` <br>
nama yang dibuat di `APP_URL` ganti menjadi Manager 


And Migrate with

`php artisan migrate --seed` or `php artisan migrate:fresh --seed`

silahkan akses website dengan 'php artisan serve'

# Login Credentials for manager<br>

## Admin
email       : admin@example.com<br>
password    : administrator

## User
email       : defaultuser@example.com<br>
password    : password<br>
email       : internuser@example.com<br>
password    : password
