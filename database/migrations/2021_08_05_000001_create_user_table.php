<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
           $table->id();
           $table->string('username');
           $table->string('password');
           $table->unsignedBigInteger('roles_id')->nullable();
           $table->string('email')->unique();
           $table->string('status');
           $table->timestamp('email_verified_at')->nullable();
           $table->timestamps();
           $table->rememberToken();

           $table->foreign('roles_id')->references('id')->on('user_roles')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
