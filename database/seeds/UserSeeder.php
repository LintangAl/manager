<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user')->delete();
        \DB::table('user')->insert(array (
            0 =>
            array (
                'id' => 1,
                'roles_id' => 1,
                'username' => 'Administrator',
                'password' =>  Hash::make('administrator'),
                'email' => 'admin@example.com',
                'status' => 'Managing',
                'email_verified_at' => now(),
                'created_at' => now(),
            ),
            1 =>
            array (
                'id' => 2,
                'roles_id' => 2,
                'username' => 'Default User',
                'password' =>  Hash::make('password'),
                'email' => 'defaultuser@example.com',
                'status' => 'onwork',
                'email_verified_at' => now(),
                'created_at' => now(),
            ),
            2 =>
            array (
                'id' => 3,
                'roles_id' => 3,
                'username' => 'Intern User',
                'password' =>  Hash::make('password'),
                'email' => 'internuser@example.com',
                'status' => 'Training',
                'email_verified_at' => now(),
                'created_at' => now(),
            ),
        ));
    }
}
