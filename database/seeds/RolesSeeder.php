<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_roles')->delete();
        \DB::table('user_roles')->insert(array(
            0 =>
            array(
                'id' => 1,
                'roles_name' => 'Administrator',
                'created_at' => now(),
            ),

            1 =>
            array(
                'id' => 2,
                'roles_name' => 'Employeer',
                'created_at' => now(),
            ),

            2 =>
            array(
                'id' => 3,
                'roles_name' => 'Intern',
                'created_at' => now(),
            ),

        ));
    }
}
