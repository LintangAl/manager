<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Roles extends Model
{
    protected $table = 'user_roles';
    protected $fillable = ['roles_name'];
    public function user()
        {
            return $this->hasMany(User::class,'roles_id','id');
        }
}
