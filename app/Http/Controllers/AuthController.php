<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    protected $redirectTo = '/';

    public function logout(Request $request){
        Auth::logout();
        return redirect('/');
    }
}
