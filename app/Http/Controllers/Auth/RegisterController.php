<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Roles;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        $roles = Roles::select()->whereNotIn('id', [1])->get();
        return view('auth.register',compact('roles'));
    }

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            'status' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:user'],
            'roles_id' => ['required', 'integer', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);
    }

      public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user)) {
            return $response;
        }
        //send  email
            $email = "manager-test@example.com";
            $rolesname = Roles::findOrFail($request->roles_id);
            $content = array(
                'username' => $request->username,
                'email' => $request->email,
                'rolesname' =>$rolesname->roles_name,
            );
            Mail::send('admin.emailNotifications', $content, function ($message) use($email){
                $message->from($email, 'Administrator');
                $message->to( $email, 'Administrator');
                $message->subject('New User!');
            });
        //send  email
        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect('profilepage');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $a)
    {
        return User::create([
            'username' => $a['username'],
            'status' => 'onwork',
            'email' => $a['email'],
            'roles_id' => $a['roles_id'],
            'password' => Hash::make($a['password']),
        ]);
    }
}
