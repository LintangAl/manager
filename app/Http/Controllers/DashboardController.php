<?php

namespace App\Http\Controllers;

use App\Roles;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile($id)
    {
            $user = User::findOrFail($id);
            return view('admin.Profile', compact('user'));
    }

    public function proUser($id)
    {
            $user = User::findOrFail($id);
            return view('admin.proUser', compact('user'));
    }

    public function index()
    {
            $data = User::paginate(10);
            $roles = Roles::get();
            return view('admin.dashboard', compact('data', 'roles'));

    }

    public function rolesIndex()
    {
        $roles = Roles::paginate(10);
        return view('admin.rolesList', compact('roles'));
    }

    public function store(array $a)
    {
        return User::create([
            'username' => $a['username'],
            'status' => $a['status'],
            'email' => $a['email'],
            'roles_id' => $a['roles_id'],
            'password' => Hash::make($a['password']),
        ]);
    }

    public function storeRole(array $a)
    {
        return Roles::create([
            'roles_name' => $a['roles_name'],
        ]);
    }

    protected function create(Request $request)
    {
        $data = $request->all();
        $this->store($data);
        return redirect(route('admin.dashboard'));
    }

    protected function createRole(Request $request)
    {
        // dd($request->all());
        $data = $request->all();
        $check = $this->storeRole($data);
        return redirect(route('dashboard.roles'));
    }

    protected function validator(array $a)
    {
        return Validator::make($a,[
            'id' => ['required','integer', 'max:255'],
            'username' => ['required','string', 'max:255'],
            'status' => ['nullable','string', 'max:255'],
            'email' => ['required','string', 'email', 'max:255', 'unique:user'],
            'roles_id' => ['required','integer', 'max:255'],
            'password' => ['required','string', 'password', 'max:255'],
        ]);
    }

    public function edit($id)
    {
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->update($request->all());
        return back();
    }

    public function updateroles(Request $request)
    {
        $role = Roles::findOrFail($request->roles_id);
        $role->update($request->all());
        return back();
    }

    public function destroy($id)
    {
        $data = User::destroy($id);
        return redirect('/dashboard')->with('status', 'User has been deleted');
    }

    public function destroyroles($id)
    {
        $role = Roles::findOrFail($id);
        $role->delete();
        return redirect(route('dashboard.roles'));
    }

    public function email($id)
    {
        $data = User::findOrFail($id);
        return view('admin.emailForm', compact('data'));
    }

    public function sendemail(Request $request)
    {
        $email = $request->email;
        $content = array(
            'username' => $request->username,
            'email_content' => $request->email_content,
        );

        //send  email
        Mail::send('admin.emailtempt', $content, function ($message) use($email){
            $message->from('manager-test@example.com', 'Administrator');
            $message->to( $email, 'no-reply');
            $message->subject('Manager-Trial');
        });

        if (Mail::failures()){
            return "FAILLLL";
        }
        return redirect('dashboard');
    }
}
