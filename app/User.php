<?php

namespace App;

use Illuminate\Foundation\Auth\User as Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class User extends Auth implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'user';

    protected $fillable = ['username', 'status','roles_id','email','password'];

    protected $hidden = ['password'];

    public function roles()
            {
               return $this->belongsTo(Roles::class, 'roles_id', 'id');
            }
}
