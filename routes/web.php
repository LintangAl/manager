<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify'=>true]);

//testing
Route::get('pro', 'HomeController@pro')->name('user.pro');

//Default route
Route::get('/', 'Auth\LoginController@index')->name('login');

//Auth route
Route::get('registration', 'Auth\RegisterController@index')->name('register');
Route::post('post-registration', 'Auth\RegisterController@register')->name('register.post');
Route::post('post-login','Auth\LoginController@login')->name('login.post');
Route::get('logout', 'AuthController@logout')->name('logout')->middleware('auth');

//Guest
Route::get('profilepage', 'HomeController@index')->name('user.profile');
Route::post('profile/update/{id}', 'HomeController@editData')->name('user.update');


//admin-only
Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin.dashboard')->middleware('roles');
Route::middleware('roles')->group( function(){
    Route::get('dashboard/roles', 'DashboardController@rolesIndex')->name('dashboard.roles');
    Route::get('dashboard/profile/{id}', 'DashboardController@proUser')->name('dashboard.profile');
    Route::post('dashboard/create', 'DashboardController@create')->name('dashboard.create');
    Route::post('dashboard/createrole', 'DashboardController@createRole')->name('dashboard.createroles');
    Route::get('dashboard/{id}/edit', 'DashboardController@edit')->name('dashboard.edit');
    Route::patch('dashboard/update', 'DashboardController@update')->name('dashboard.update');
    Route::patch('dashboard/updateRoles', 'DashboardController@updateroles')->name('dashboard.updateroles');
    Route::get('dashboard/destroy/{id}', 'DashboardController@destroy')->name('dashboard.destroy');
    Route::get('dashboard/destroyRoles/{id}', 'DashboardController@destroyroles')->name('dashboard.destroyroles');
    Route::get('/emailform/{id}', 'DashboardController@email')->name('email.form');
    Route::post('/sendemail', 'DashboardController@sendemail')->name('email.send');
});
